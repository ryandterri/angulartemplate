/// <vs SolutionOpened='watch:coretrac' />
module.exports = function (grunt) {

    grunt.initConfig({
        concat: {
            angular: {
                src: ['app/*.js', 'app/modules/**/*.js'],
                dest: 'app/scripts/app.js'
            },
            core: {
                src: ['bower_components/angular/angular.js'
                    , 'bower_components/angular-ui-router/release/angular-ui-router.js'
                    , 'bower_components/jquery/dist/jquery.js'
                    , 'bower_components/bootstrap/dist/js/bootstrap.js'
                    , 'bower_components/underscore/underscore.js'
                    , 'bower_components/angular-filter/dist/angular-filter.js'
                ],
                dest: 'app/scripts/core.js'
            },
            css: {
                src: ['css/bootstrap.css'
                    , 'css/font-awesome.css'
                    , 'css/custom.css'
                ],
                dest: 'app/css/core.css'
            }
        },
        less: {
            bootstrap: {
                files: {'css/bootstrap.css': 'less/bootstrap.less'}
            },
            custom: {
                files: {'css/custom.css': 'less/custom.less'}
            },
            fontawesome: {
                files: {'css/font-awesome.css': 'bower_components/font-awesome/less/font-awesome.less'}
            }
        },
        uglify: {
            min: {
                files: {
                    'app/scripts/app.min.js': ['app/scripts/app.js'],
                    'app/scripts/core.min.js': ['app/scripts/core.js']
                }
            }
        },
        cssmin: {
            css: {
                files: {
                    'app/css/core.min.css': ['app/css/core.css']
                }
            }
        },
        copy: {
            fonts: {
                expand: true,
                flatten: true,
                src: ['bower_components/bootstrap/dist/fonts/*', 'bower_components/font-awesome/fonts/*'],
                dest: 'app/fonts/'
            }
        },
        watch: {
            core: {
                files: ['app/scripts/*.js'],
                tasks: ['uglify:min']
            },
            angular: {
                files: ['app/*.js', 'app/modules/**/*.js'],
                tasks: ['concat:angular']
            },
            css: {
                files: ['less/**/*.less'],
                tasks: ['less', 'concat:css', 'cssmin']
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    grunt.registerTask('dist', ['concat', 'less', 'uglify', 'cssmin', 'copy'])
};